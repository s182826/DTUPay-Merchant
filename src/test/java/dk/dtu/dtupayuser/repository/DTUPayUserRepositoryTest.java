package dk.dtu.dtupayuser.repository;

import dk.dtu.dtupayuser.exception.DTUPayUserNotFoundException;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.DTUPayUser;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 *  @author Bence Burom (s192620)
 */
public class DTUPayUserRepositoryTest {

    private static final CprNumber WRONG_CPR = new CprNumber("wrongCpr");
    private static final String FIRST_NAME = "FirstName";
    private static final String LAST_NAME = "LastName";
    private static final CprNumber CPR = new CprNumber("cpr");
    private static final String NEW_FIRST_NAME = "NewFirstName";
    private DTUPayUserRepository dtuPayUserRepository;
    private Customer newCustomer;

    @Before
    public void setup() {
        dtuPayUserRepository = new DTUPayUserRepository();
        newCustomer = new Customer(CPR, FIRST_NAME, LAST_NAME, "");
        dtuPayUserRepository.createUser(newCustomer);
    }

   @Test
   public void findCustomerByCprNumber(){
       DTUPayUser foundCustomer = dtuPayUserRepository.findByCprNumber(CPR);
       assertEquals(newCustomer, foundCustomer);
       assertEquals(FIRST_NAME, foundCustomer.getFirstName());
       assertEquals(LAST_NAME, foundCustomer.getLastName());
   }

    @Test (expected = DTUPayUserNotFoundException.class)
    public void exceptionWhenCustomerNotExist(){
        DTUPayUser foundCustomer = dtuPayUserRepository.findByCprNumber(WRONG_CPR);
    }

    @Test
    public void updateCustomer(){
        DTUPayUser foundCustomer = dtuPayUserRepository.findByCprNumber(CPR);
        foundCustomer.setFirstName(NEW_FIRST_NAME);
        dtuPayUserRepository.updateUser(foundCustomer);
        DTUPayUser updatedCustomer = dtuPayUserRepository.findByCprNumber(CPR);
        assertEquals(NEW_FIRST_NAME, updatedCustomer.getFirstName());
        assertEquals(LAST_NAME, updatedCustomer.getLastName());
    }

    @Test (expected = DTUPayUserNotFoundException.class)
    public void deleteCustomer(){
        DTUPayUser foundCustomer = dtuPayUserRepository.findByCprNumber(CPR);
        dtuPayUserRepository.deleteUser(foundCustomer);
        dtuPayUserRepository.findByCprNumber(CPR);
    }

    @Test
    public void getByIdTest() {
        Customer customer1 = getACustomer();
        Customer customer2 = getACustomer();
        Customer customer3 = getACustomer();

        dtuPayUserRepository.createUser(customer1);
        dtuPayUserRepository.createUser(customer2);
        dtuPayUserRepository.createUser(customer3);

        DTUPayUser found = dtuPayUserRepository.findByCprNumber(customer1.getCprNumber());
        assertEquals(customer1, found);
    }

    private Customer getACustomer() {
        return new Customer(
                new CprNumber(UUID.randomUUID().toString()),
                "test",
                "test",
                "test"
        );
    }

}
