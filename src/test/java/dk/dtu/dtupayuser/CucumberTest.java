package dk.dtu.dtupayuser;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/dk/dtu/dtupayuser/service/feature",
        monochrome = true,
        snippets = SnippetType.CAMELCASE,
        glue = "dk.dtu.dtupayuser.service.step"
)
public class CucumberTest {
}
