package dk.dtu.dtupayuser.service;

import dk.dtu.dtupayuser.exception.DTUPayUserNotFoundException;
import dk.dtu.dtupayuser.message.UserCreationMessageSender;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.DTUPayUser;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.repository.DTUPayUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
/**
 *  @author Janos Richard Pekk (s192617)
 */
@RunWith(MockitoJUnitRunner.class)
public class DTUPayUserServiceTest {

    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String LAST_NAME_NEW = "LAST_NAME_NEW";
    private DTUPayUserService dtuPayUserService;
    private DTUPayUser customer;
    private DTUPayUser merchant;

    private UserCreationMessageSender mockUserCreationMessageSender = mock(UserCreationMessageSender.class);

    @Before
    public void init() {
        dtuPayUserService = new DTUPayUserService(new DTUPayUserRepository(), mockUserCreationMessageSender);
        customer = new Customer(
                new CprNumber(UUID.randomUUID().toString()),
                FIRST_NAME,
                LAST_NAME,
                "123");

        merchant  = new Customer(
                new CprNumber(UUID.randomUUID().toString()),
                FIRST_NAME,
                LAST_NAME,
                "321");
    }

    @Test
    public void createCustomerAndGetByCpr() {
        doNothing().when(mockUserCreationMessageSender).sendCustomerCreation(customer.getCprNumber().getNumber(),customer.getFirstName(),customer.getLastName());
        dtuPayUserService.createCustomer(customer);
        DTUPayUser savedUser = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        assertEquals(savedUser, customer);
    }

    @Test
    public void createCustomerAndGetByAccountNo() {
        dtuPayUserService.createCustomer(customer);
        DTUPayUser savedUser = dtuPayUserService.getUserByAccountNo(customer.getAccountNo());
        assertEquals(savedUser, customer);
    }

    @Test
    public void createAndUpdateCustomer() {
        dtuPayUserService.createCustomer(customer);

        DTUPayUser updatedCustomer = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        updatedCustomer.setLastName(LAST_NAME_NEW);
        dtuPayUserService.updateUser(updatedCustomer);

        DTUPayUser savedUser = dtuPayUserService.getUserByCprNumber(updatedCustomer.getCprNumber());
        assertEquals(LAST_NAME_NEW, savedUser.getLastName());
    }

    @Test(expected = DTUPayUserNotFoundException.class)
    public void createAndDeleteCustomer() {
        dtuPayUserService.createCustomer(customer);
        dtuPayUserService.deleteUser(customer.getCprNumber().getNumber());

        dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
    }

    @Test
    public void createMerchantAndGetByCpr() {
        dtuPayUserService.createMerchant(merchant);
        DTUPayUser savedUser = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        assertEquals(savedUser, merchant);
    }

    @Test
    public void createMerchantAndGetByAccountNo() {
        dtuPayUserService.createMerchant(merchant);
        DTUPayUser savedUser = dtuPayUserService.getUserByAccountNo(merchant.getAccountNo());
        assertEquals(savedUser, merchant);
    }

    @Test
    public void createAndUpdateMerchant() {
        dtuPayUserService.createMerchant(merchant);

        DTUPayUser updatedMerchant = new Customer(
                merchant.getCprNumber(),
                merchant.getFirstName(),
                merchant.getLastName(),
                "456");
        dtuPayUserService.updateUser(updatedMerchant);

        DTUPayUser savedUser = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        assertNotEquals(savedUser, merchant);
    }

    @Test(expected = DTUPayUserNotFoundException.class)
    public void createAndDeleteMerchant() {
        dtuPayUserService.createMerchant(merchant);
        dtuPayUserService.deleteUser(merchant.getCprNumber().getNumber());

        dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
    }

}
