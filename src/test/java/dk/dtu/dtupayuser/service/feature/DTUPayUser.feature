Feature: DTUPayUser
  Description: Managing DTUPayUsers

  Scenario: Creating a new customer
    Given a customer
    When adding the customer to the system
    Then the customer gets saved

  Scenario: Modifying a customer
    Given a customer
    When adding the customer to the system
    And modifying that customer
    Then the customer gets modified

  Scenario: Getting a saved customer by account number
    Given a customer
    When adding the customer to the system
    Then the customer can be found by account number

  Scenario: Getting a saved merchant by account number
    Given a merchant
    When adding the merchant to the system
    Then the merchant can be found by account number

  Scenario: Creating a new merchant
    Given a merchant
    When adding the merchant to the system
    Then the merchant gets saved

  Scenario: Modifying a merchant
    Given a merchant
    When adding the merchant to the system
    And modifying that merchant
    Then the merchant gets modified

  Scenario: Delete customer
    Given a customer
    When adding the customer to the system
    And deleting the customer from the system
    Then the customer gets deleted

  Scenario: Delete merchant
    Given a merchant
    When adding the merchant to the system
    And deleting the merchant from the system
    Then the merchant gets deleted

