package dk.dtu.dtupayuser.service.step;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.dtu.dtupayuser.exception.DTUPayUserNotFoundException;
import dk.dtu.dtupayuser.message.UserCreationMessageSender;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.DTUPayUser;
import dk.dtu.dtupayuser.model.Merchant;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.repository.DTUPayUserRepository;
import dk.dtu.dtupayuser.service.DTUPayUserService;
import org.assertj.core.api.Assertions;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

/**
 *  @author Daniel Szajcz (s192623)
 */
public class DTUPayUserSteps {

    private Customer customer;
    private Merchant merchant;

    private String modification = "modification";

    private DTUPayUserService dtuPayUserService;

    public DTUPayUserSteps() {
        UserCreationMessageSender mockUserCreationMessageSender = mock(UserCreationMessageSender.class);
        this.dtuPayUserService = new DTUPayUserService(new DTUPayUserRepository(), mockUserCreationMessageSender);
    }

    @Given("a customer")
    public void getACustomer() {
        customer = new Customer(
                new CprNumber(UUID.randomUUID().toString()),
                "test",
                "test",
                "test"
        );
    }

    @When("adding the customer to the system")
    public void addingTheCustomerToTheSystem() {
        dtuPayUserService.createCustomer(customer);
    }

    @Then("the customer gets saved")
    public void theCustomerGetsSaved() {
        DTUPayUser requestedCustomer = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        assertEquals(customer, requestedCustomer);
    }

    @And("modifying that customer")
    public void modifyingThatCustomer() {
        DTUPayUser customerToUpdate = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        customerToUpdate.setLastName(modification);
        dtuPayUserService.updateUser(customerToUpdate);
    }

    @Then("the customer gets modified")
    public void theCustomerGetsModified() {
        DTUPayUser updatedCustomer = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        assertEquals(modification, updatedCustomer.getLastName());
    }

    @Given("a merchant")
    public void getAMerchant() {
        merchant = new Merchant(
                new CprNumber(UUID.randomUUID().toString()),
                "test",
                "test",
                "test"
        );
    }

    @When("adding the merchant to the system")
    public void addingTheMerchantToTheSystem() {
        dtuPayUserService.createMerchant(merchant);
    }

    @Then("the merchant gets saved")
    public void theMerchantGetsSaved() {
        DTUPayUser requestedMerchant = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        assertEquals(merchant, requestedMerchant);
    }

    @And("modifying that merchant")
    public void modifyingThatMerchant() {
        DTUPayUser merchantToUpdate = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        merchantToUpdate.setLastName(modification);
        dtuPayUserService.updateUser(merchantToUpdate);
    }

    @Then("the merchant gets modified")
    public void theMerchantGetsModified() {
        DTUPayUser updatedCustomer = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        assertEquals(modification, updatedCustomer.getLastName());
    }

    @And("deleting the customer from the system")
    public void deletingTheCustomerFromTheSystem() {
        DTUPayUser customerToDelete = dtuPayUserService.getUserByCprNumber(customer.getCprNumber());
        dtuPayUserService.deleteUser(customerToDelete.getCprNumber().getNumber());
    }


    @Then("the customer gets deleted")
    public void theCustomerGetsDeleted() {
        Assertions.assertThatThrownBy(() -> dtuPayUserService.getUserByCprNumber(customer.getCprNumber())).
                isInstanceOf(DTUPayUserNotFoundException.class);
    }

    @And("deleting the merchant from the system")
    public void deletingTheMerchantFromTheSystem() {
        DTUPayUser merchantToDelete = dtuPayUserService.getUserByCprNumber(merchant.getCprNumber());
        dtuPayUserService.deleteUser(merchantToDelete.getCprNumber().getNumber());
    }


    @Then("the merchant gets deleted")
    public void theMerchantGetsDeleted() {
        Assertions.assertThatThrownBy(() -> dtuPayUserService.getUserByCprNumber(merchant.getCprNumber())).
                isInstanceOf(DTUPayUserNotFoundException.class);
    }

    @Then("^the customer can be found by account number$")
    public void theUserCanBeFoundByAccountNumber() {
        DTUPayUser requestedMerchant = dtuPayUserService.getUserByAccountNo(customer.getAccountNo());
        assertEquals(customer.getCprNumber(), requestedMerchant.getCprNumber());
    }

    @Then("^the merchant can be found by account number$")
    public void theMerchantCanBeFoundByAccountNumber() {
        DTUPayUser requestedMerchant = dtuPayUserService.getUserByAccountNo(merchant.getAccountNo());
        assertEquals(merchant.getCprNumber(), requestedMerchant.getCprNumber());
    }
}
