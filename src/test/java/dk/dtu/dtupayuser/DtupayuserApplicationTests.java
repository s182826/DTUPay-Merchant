package dk.dtu.dtupayuser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.Merchant;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
/**
 *  @author Janos Richard Pekk (s192617)
 *  @author Daniel Szajcz (s192623)
 *  @author Bence Burom (s192620)
 */
@SpringBootTest
class DtupayuserApplicationTests {

	@Autowired
	ObjectMapper objectMapper;

	@Test
	void customerSer() throws IOException {
		Customer customer = new Customer(new CprNumber("asd"), "asd", "asd", "asd");

		String ser = objectMapper.writeValueAsString(customer);

		assertNotNull(ser);


		Customer deser = objectMapper.readValue(ser, Customer.class);

		assertNotNull(deser);
	}

	@Test
	void merchantSer() throws IOException  {
		Merchant merchant = new Merchant(new CprNumber("asd"), "asd", "asd", "asd");

		String ser = objectMapper.writeValueAsString(merchant);

		assertNotNull(ser);

		Merchant deser = objectMapper.readValue(ser, Merchant.class);

		assertNotNull(deser);
	}

}
