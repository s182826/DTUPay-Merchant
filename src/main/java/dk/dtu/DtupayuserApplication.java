package dk.dtu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtupayuserApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtupayuserApplication.class, args);
	}


}
