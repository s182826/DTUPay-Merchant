package dk.dtu.dtupayuser.repository;

import dk.dtu.dtupayuser.model.Merchant;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.exception.DTUPayUserNotFoundException;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.DTUPayUser;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Component
public class DTUPayUserRepository implements IDTUPayUserRepository {

    private List<DTUPayUser> dtuPayUsers = new ArrayList<>();

    @Override
    public DTUPayUser findByCprNumber(CprNumber cprNumber) {
        DTUPayUser foundUser = dtuPayUsers.stream()
                .filter(m -> m.getCprNumber().getNumber().equals(cprNumber.getNumber()))
                .findFirst()
                .orElseThrow(() -> new DTUPayUserNotFoundException(cprNumber));

        return instantiateUser(foundUser);
    }

    @Override
    public DTUPayUser findByAccountNo(String accountNo) {
        DTUPayUser foundUser = dtuPayUsers.stream()
                .filter(m -> m.getAccountNo().equals(accountNo))
                .findFirst()
                .orElseThrow(() -> new DTUPayUserNotFoundException(accountNo));

        return instantiateUser(foundUser);
    }

    @Override
    public void createUser(DTUPayUser user) {
        dtuPayUsers.add(user);
    }

    @Override
    public void deleteUser(DTUPayUser user) {
        dtuPayUsers.remove(user);
    }

    @Override
    public void updateUser(DTUPayUser updatedUser) {
        dtuPayUsers.set(dtuPayUsers.indexOf(dtuPayUsers.stream()
                .filter(u -> u.getCprNumber().getNumber().equals(updatedUser.getCprNumber().getNumber()))
                .findFirst()
                .orElseThrow(() -> new DTUPayUserNotFoundException(updatedUser.getCprNumber()))), updatedUser);
    }

    @Override
    public void setAccountNo(String cprNumber, String accountNo) {
        DTUPayUser foundUser = dtuPayUsers.stream()
                .filter(u -> u.getCprNumber().getNumber().equals(cprNumber))
                .findFirst()
                .orElseThrow(() -> new DTUPayUserNotFoundException(cprNumber));
        foundUser.setAccountNo(accountNo);
    }

    private DTUPayUser instantiateUser(DTUPayUser foundUser) {
        DTUPayUser dtuPayUser = foundUser instanceof Customer ?
                new Customer() : new Merchant();

        dtuPayUser.setAccountNo(foundUser.getAccountNo());
        dtuPayUser.setCprNumber(foundUser.getCprNumber());
        dtuPayUser.setFirstName(foundUser.getFirstName());
        dtuPayUser.setLastName(foundUser.getLastName());

        return dtuPayUser;
    }
}
