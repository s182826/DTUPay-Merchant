package dk.dtu.dtupayuser.repository;

import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.model.DTUPayUser;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public interface IDTUPayUserRepository {

    DTUPayUser findByCprNumber(CprNumber cprNumber);
    DTUPayUser findByAccountNo(String accountNo);
    void createUser(DTUPayUser user);
    void deleteUser(DTUPayUser user);
    void updateUser(DTUPayUser user);
    void setAccountNo(String cprNumber, String accountNo);


}
