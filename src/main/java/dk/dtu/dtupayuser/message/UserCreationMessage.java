package dk.dtu.dtupayuser.message;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 *  @author Christoffer Svendsen (s145089)
 */

public final class UserCreationMessage implements Serializable {

    private final String cprNumber;
    private final String firstName;
    private final String lastName;

    public UserCreationMessage(@JsonProperty("cpr") String cprNumber,
                               @JsonProperty("firstName") String firstName,
                               @JsonProperty("lastName") String lastName) {
        this.cprNumber = cprNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getCprNumber() {
        return cprNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "UserCreationMessage{" +
                "cprNumber='" + cprNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
