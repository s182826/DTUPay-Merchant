package dk.dtu.dtupayuser.message;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Configuration
public class UserMQConfiguration {

    public static final String BANK_EXCHANGE_NAME = "bank-user-exchange";
    public static final String BANK_ROUTING_KEY = "bank-user-exchange-create-user-routing-key";
    public static final String TRANSACTION_EXCHANGE_NAME = "transaction-user-exchange";
    public static final String TRANSACTION_ROUTING_KEY = "transaction-user-exchange-add-cpr-routing-key";
    public static final String TOKEN_EXCHANGE_NAME = "token-user-exchange";
    public static final String TOKEN_ROUTING_KEY = "token-user-exchange-add-cpr-routing-key";
    public static final String BANK_MERCHANT_EXCHANGE = "bank-merchant-exchange";
    public static final String BANK_MERCHANT_ROUTING_KEY = "bank-merchant-exchange-create-merchant-routing-key";
    public static final String TRANSACTION_MERCHANT_EXCHANGE = "transaction-merchant-exchange";
    public static final String TRANSACTION_MERCHANT_ROUTING_KEY = "transaction-merchant-routing-key";

    public static final String BANK_ACCOUNT_NO_EXCHANGE = "user-account-no-exchange";
    public static final String BANK_ACCOUNT_NO_ROUTING_KEY = "user-account-no-exchange-routing-key";
    public static final String BANK_ACCOUNT_NO_QUEUE = "userAccountNoQueue";

    @Bean
    public TopicExchange appExchange() {
        return new TopicExchange(BANK_ACCOUNT_NO_EXCHANGE);
    }

    @Bean
    public Queue transactionQueue() { return new Queue(BANK_ACCOUNT_NO_QUEUE); }

    @Bean
    public Binding declareBindingTransaction() {
        return BindingBuilder.bind(transactionQueue()).to(appExchange()).with(BANK_ACCOUNT_NO_ROUTING_KEY);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
