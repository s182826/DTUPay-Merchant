package dk.dtu.dtupayuser.message;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
@Log4j2
public class UserCreationMessageSender {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public UserCreationMessageSender(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendCustomerCreation(String cprNumber, String firstName, String lastName) {
        UserCreationMessage message = new UserCreationMessage(cprNumber, firstName, lastName);
        log.info("Creating customer: " + message);
        rabbitTemplate.convertAndSend(UserMQConfiguration.BANK_EXCHANGE_NAME, UserMQConfiguration.BANK_ROUTING_KEY, message);
        rabbitTemplate.convertAndSend(UserMQConfiguration.TRANSACTION_EXCHANGE_NAME, UserMQConfiguration.TRANSACTION_ROUTING_KEY, message);
        rabbitTemplate.convertAndSend(UserMQConfiguration.TOKEN_EXCHANGE_NAME, UserMQConfiguration.TOKEN_ROUTING_KEY, message);
    }

    public void sendMerchantCreation(String cprNumber, String firstName, String lastName) {
        UserCreationMessage message = new UserCreationMessage(cprNumber, firstName, lastName);
        log.info("Creating merchant: " + message);
        rabbitTemplate.convertAndSend(UserMQConfiguration.BANK_MERCHANT_EXCHANGE, UserMQConfiguration.BANK_MERCHANT_ROUTING_KEY, message);
        rabbitTemplate.convertAndSend(UserMQConfiguration.TRANSACTION_MERCHANT_EXCHANGE, UserMQConfiguration.TRANSACTION_MERCHANT_ROUTING_KEY, message);
    }
}
