package dk.dtu.dtupayuser.message;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dk.dtu.dtupayuser.service.DTUPayUserService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
public class MessageListener {

    private final DTUPayUserService dtuPayUserService;
    private final Gson gson;

    public MessageListener(DTUPayUserService dtuPayUserService) {
        this.dtuPayUserService = dtuPayUserService;
        this.gson = new Gson();
    }

    @RabbitListener(queues = "userAccountNoQueue")
    public void userAccountNo(final Message message) {
        System.out.println("Message: " + message);
        String body = new String(message.getBody());
        System.out.println(body);
        JsonObject jsonObject = gson.fromJson(body, JsonObject.class);
        dtuPayUserService.setAccountNo(jsonObject.get("cprNumber").getAsString(), jsonObject.get("accountNo").getAsString());
    }


}
