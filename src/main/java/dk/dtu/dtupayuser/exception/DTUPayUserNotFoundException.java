package dk.dtu.dtupayuser.exception;

import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DTUPayUserNotFoundException extends RuntimeException {
    public DTUPayUserNotFoundException(CprNumber cprNumber){
        super("User not found with cpr number: " + cprNumber.getNumber());
    }

    public DTUPayUserNotFoundException(String accountNo){
        super("User not found with account no: " + accountNo);
    }
}
