package dk.dtu.dtupayuser.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Data
@NoArgsConstructor
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Customer.class, name = "Customer"),
        @JsonSubTypes.Type(value = Merchant.class, name = "Merchant")
})
public abstract class DTUPayUser {
    protected String type;
    private CprNumber cprNumber;
    private String firstName;
    private String lastName;
    private String accountNo;

    public DTUPayUser(CprNumber cprNumber, String firstName, String lastName, String accountNo){
        this.cprNumber = cprNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNo = accountNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DTUPayUser that = (DTUPayUser) o;
        return Objects.equals(cprNumber, that.cprNumber) &&
                Objects.equals(accountNo, that.accountNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cprNumber, accountNo);
    }

    @Override
    public String toString() {
        return "Cpr number: " + cprNumber.getNumber() +
                ", Name: " + firstName + " " + lastName +
                ", Account no: " + accountNo;
    }
}
