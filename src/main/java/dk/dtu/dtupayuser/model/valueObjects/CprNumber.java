package dk.dtu.dtupayuser.model.valueObjects;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@NoArgsConstructor
public class CprNumber {

    @Setter(AccessLevel.NONE)
    private String number;

    public CprNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CprNumber cprNumber = (CprNumber) o;
        return number.equals(cprNumber.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
