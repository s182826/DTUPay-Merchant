package dk.dtu.dtupayuser.model;

import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import lombok.NoArgsConstructor;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@NoArgsConstructor
public class Customer extends DTUPayUser{
    public Customer(CprNumber cprNumber, String firstName, String lastName, String accountNo) {
        super(cprNumber, firstName, lastName, accountNo);
        type = "Customer";
    }
}
