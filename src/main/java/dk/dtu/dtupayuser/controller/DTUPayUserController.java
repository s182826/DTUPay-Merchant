package dk.dtu.dtupayuser.controller;

import dk.dtu.dtupayuser.model.Merchant;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.service.DTUPayUserService;
import dk.dtu.dtupayuser.model.Customer;
import dk.dtu.dtupayuser.model.DTUPayUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@RestController
@RequestMapping(path = "/api/user")
@Log4j2
public class DTUPayUserController {

    private final DTUPayUserService dtuPayUserService;

    @Autowired
    public DTUPayUserController(DTUPayUserService dtuPayUserService) {
        this.dtuPayUserService = dtuPayUserService;
    }

    @GetMapping(path = "/{cprNumber}")
    public DTUPayUser getUserByCprNumber(@PathVariable CprNumber cprNumber) {
        log.info("Requesting user with cpr number: " + cprNumber.getNumber());
        return dtuPayUserService.getUserByCprNumber(cprNumber);
    }

    @PostMapping(path = "/customer")
    public void addCustomer(@RequestBody Customer customer) {
        log.info("Creating customer: " + customer);
        dtuPayUserService.createCustomer(customer);
    }

    @PostMapping(path = "/merchant")
    public void addMerchant(@RequestBody Merchant merchant) {
        log.info("Creating merchant: " + merchant);
        dtuPayUserService.createMerchant(merchant);
    }

    @PutMapping
    public void updateUser(@RequestBody DTUPayUser user) {
        log.info("Updating user: " + user);
        dtuPayUserService.updateUser(user);
    }

    @DeleteMapping(path = "/{cprNumber}")
    public void deleteUser(@PathVariable String cprNumber) {
        log.info("Deleting user with cpr number: " + cprNumber);
        dtuPayUserService.deleteUser(cprNumber);
    }


}
