package dk.dtu.dtupayuser.service;

import dk.dtu.dtupayuser.message.UserCreationMessageSender;
import dk.dtu.dtupayuser.model.DTUPayUser;
import dk.dtu.dtupayuser.model.valueObjects.CprNumber;
import dk.dtu.dtupayuser.repository.IDTUPayUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *  @author Christoffer Svendsen (s145089)
 */

@Service
public class DTUPayUserService {

    private final IDTUPayUserRepository dtuPayUserRepository;
    private final UserCreationMessageSender userCreationMessageSender;

    @Autowired
    public DTUPayUserService(IDTUPayUserRepository dtuPayUserRepository, UserCreationMessageSender userCreationMessageSender) {
        this.dtuPayUserRepository = dtuPayUserRepository;
        this.userCreationMessageSender = userCreationMessageSender;
    }

    public DTUPayUser getUserByCprNumber(CprNumber cprNumber) {
        return dtuPayUserRepository.findByCprNumber(cprNumber);
    }

    public DTUPayUser getUserByAccountNo(String accountNo) {
        return dtuPayUserRepository.findByAccountNo(accountNo);
    }

    public void createCustomer(DTUPayUser user) {
        dtuPayUserRepository.createUser(user);
        userCreationMessageSender.sendCustomerCreation(user.getCprNumber().getNumber(), user.getFirstName(), user.getLastName());
    }

    public void createMerchant(DTUPayUser user) {
        dtuPayUserRepository.createUser(user);
        userCreationMessageSender.sendMerchantCreation(user.getCprNumber().getNumber(), user.getFirstName(), user.getLastName());
    }

    public void updateUser(DTUPayUser user) {
        dtuPayUserRepository.updateUser(user);
    }

    public void setAccountNo(String cprNumber, String accountNo) {
        dtuPayUserRepository.setAccountNo(cprNumber, accountNo);
    }

    public void deleteUser(String cprNumber) {
        DTUPayUser user = dtuPayUserRepository.findByCprNumber(new CprNumber(cprNumber));
        dtuPayUserRepository.deleteUser(user);
    }

}
