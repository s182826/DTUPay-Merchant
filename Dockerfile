FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/user-0.0.1-SNAPSHOT.jar /usr/src
EXPOSE 8080
CMD java -jar user-0.0.1-SNAPSHOT.jar
